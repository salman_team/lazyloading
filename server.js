const cluster = require('cluster'),
  os = require('os'),
  app = require('./app');


if (cluster.isMaster) {
  let i,
    numberProcess = process.env.NODE_ENV === 'production' ? os.cpus().length : 1;

  if (numberProcess > 1) {
    for (i = 0; i < numberProcess; i++) {
      cluster.fork();
    }
    cluster.on('exit', function () {

    })
  } else {
    (new app().boostrap())
  }

}else {
  (new app().boostrap())
}