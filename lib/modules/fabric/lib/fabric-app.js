const express = require('express'),
  bodyParser = require('body-parser'),
  Constants = require('../common/constants'),
  path = require('path'),
  Logger = require('./logger');


class FabricApp {
  constructor() {
    this.dependecies = {};
    this.appData = {};
    this.appData.dependecies = {};
    this.cwd = process.cwd();
    this.loadConfigs();
    this.initializeLogger();
  }

  boostrap() {
    try {
      this.createSeverInstance();
      this.setAppSpecifications();
      this.loadDependencies();
      this.registerRouter();
      this.registerRenderDirect(this.appData.dependecies.app);
    } catch (e) {
      throw e;
    }
  }

  loadConfigs() {
    this.config = require(path.join(this.cwd, Constants.configPath));
    console.log(`config loaded`, this.config);
  }

  createSeverInstance() {
    let app = this.app = express();
    this.appData.dependecies.app = app;
    app.listen(this.config.port || 8099);
  }

  setAppSpecifications() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  initializeLogger() {
    if (this.config.logger !== Constants.console)
      this.appData.dependecies.logger = new Logger();
    else
      this.appData.dependecies.logger = console;
  }

  registerRouter() {
    const router = require(path.join(this.cwd, Constants.routerPath));
    new router(this.config, this.dependecies).initRouters(this.appData.dependecies.app)
  }

  loadDependencies() {

  }
}

module.exports = FabricApp