'use strict'

class BaseHelper {
  constructor(config, dependencies, requestContext) {
    this.config = config;
    this.dependencies = dependencies;
    this.requestContext = requestContext;
  }

  setupContext() {
    this.logger = this.dependencies.logger;
  }

  log(scope, methodName, message, paylaod, args = {}) {
    this.logger.log(scope, methodName, message, JSON.stringify(paylaod), args = {});
  }
}