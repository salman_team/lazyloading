const Constants = {
  configPath: 'lib/config/config',
  routerPath: 'lib/routes/router',
  console: 'console'
}


module.exports = Constants;