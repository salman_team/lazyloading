'use strict'

class Routers {
  constructor(configs, dependecies) {
    this.configs = configs;
    this.dependecies = dependecies;
  }

  initRouters(app) {
    app.get('/user', function (req, res) {
      console.log(req.path)
      res.status(200).send('welcome');
    });

    app.post('/user', function (req, res) {
      console.log(req.body)
      res.status(200).send({ user: "user" });
    });
  }
}

module.exports = Routers;