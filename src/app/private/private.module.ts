import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { Dashboard } from './components/dashboard/dashboard.component';
import { Inventory } from './components/inventory/inventory.component';
import { Orders } from './components/orders/orders.component';
import { Menus } from './components/menus/menus.component';
import { Usermanagment } from './components/usermanagment/usermanagment.component';
import { PrivateLayout } from './private-layout.component';
import { PrivateHeader } from './components/private-header/private-header.component';

const PrivateRouter: Routes = [
  {
    path: 'admin', component: PrivateLayout, children: [
      { path: '', pathMatch:'full', redirectTo: 'dashboard' },
      { path: 'dashboard', component: Dashboard },
      { path: 'inventory', component: Inventory },
      { path: 'orders', component: Orders },
      { path: 'usermanagment', component: Usermanagment },
      { path: 'menus', component: Menus }
    ]
  }

]

@NgModule({
  declarations: [
    PrivateLayout,
    Dashboard,
    Inventory,
    Orders,
    Menus,
    PrivateHeader,
    Usermanagment
  ],
  imports: [
    RouterModule,
    CommonModule,
    RouterModule.forChild(PrivateRouter)
  ],
  providers: [],
  exports: [
    RouterModule,
    CommonModule,
    PrivateLayout,
    PrivateHeader
  ]
})

export class PrivateModule {
  constructor() {

  }
}