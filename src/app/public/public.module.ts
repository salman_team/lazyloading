import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { Home } from './components/home/home.component';
import { Aboutus } from './components/aboutus/aboutus.component';
import { Login } from './components/login/login.component';
import { Signup } from './components/signup/signup.component';
import { CommonModule } from '@angular/common';
import { Gallary } from './components/gallary/gallary.component';
import { Public } from './public.component'
import { PublicHeader } from './components/public-header/public-header.component';
import { PublicFooter } from './components/public-footer/public-footer.component';

const PublicRouter: Routes = [
  {
    path: '', component: Public, children: [
      { path: '', pathMatch: 'full', redirectTo: '/login' },
      { path: 'home', component: Home },
      { path: 'aboutus', component: Aboutus },
      { path: 'login', component: Login },
      { path: 'gallary', component: Gallary },
      { path: 'signup', component: Signup }
    ]
  }
]


@NgModule({
  declarations: [
    Home,
    Aboutus,
    Login,
    Signup,
    Gallary,
    Public,
    PublicHeader,
    PublicFooter
  ],
  imports: [
    RouterModule.forChild(PublicRouter),
    FormsModule,
    CommonModule
  ],
  providers: [],
  exports: [
    RouterModule,
    Public,
    PublicHeader,
    PublicFooter
  ]
})

export class PublicModule {

}