import { Component } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class Login {
  constructor(private ActivRouter:ActivatedRoute, private router:Router) {
 
  }

  login(){
    this.router.navigateByUrl('/admin');
  }
}