import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicModule } from './public/public.module';
import { PrivateModule } from './private/private.module'


import { AppComponent } from './app.component';

const MyRouters: Routes = [
  { path: '', loadChildren: './public/public.module#PublicModule' },
  { path: 'admin', loadChildren: './private/private.module#PrivateModule' }
]

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    PublicModule,
    PrivateModule,
    RouterModule.forRoot(MyRouters)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
