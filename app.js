const express = require('express');
const path = require('path');
const FabricApp = require('./lib/modules/fabric').FabricApp

class App extends FabricApp {
  constructor() {
    super();
  }

  loadDependencies() {
    console.log(this.config);
  }

  registerRenderDirect(app) {
    if (this.config.renderAvailable) {
      app.use(express.static(path.join(__dirname, 'dist/lazyloading')));
      app.get('/*',function (req, res) {
        res.sendFile(path.join(__dirname, 'dist/lazyloading', 'index.html'))
      })
    }
  }
}

module.exports = App;